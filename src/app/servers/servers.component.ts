import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  private allowNewServer: boolean = false;
  serverCreatingStatus: string = '';

  getAllowNewServer(): boolean {
    return this.allowNewServer;
  }

  getServerCreatingStatus(): string {
    return this.serverCreatingStatus;
  }
  changeServerStatus(text: any) {
    console.log(text);

    this.serverCreatingStatus = text.target.value;

    // this.serverCreatingStatus = text;
  }

  onCreateServer(): void {
    console.log('test');
    this.serverCreatingStatus = 'now was creating server !';
  }

  constructor() {
    setTimeout(
      () => {
        this.allowNewServer = false;
      }, 2000);
  }

  ngOnInit(): void {}


}
